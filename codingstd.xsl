<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" />
<xsl:template match="/">

    <html>
    <head>
        <link href="codingstd.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <a name="top"/>
        <h3>PS Coding Standard</h3>
        Table of Content
        <ul>
        <xsl:for-each select="codingstandard/section">
            <li>
                <a>
                    <xsl:attribute name="href">#<xsl:value-of select="translate(@title,' ','')"/></xsl:attribute>
                    <xsl:value-of select="@title"/>
                </a>
            </li>
            <ul>
                <xsl:for-each select="item">
                <li>
                    <a>
                        <xsl:attribute name="href">#Item-<xsl:value-of select="translate(@title,' ','')"/></xsl:attribute>
                        <xsl:value-of select="@title"/>
                    </a>
                </li>
                </xsl:for-each>
            </ul>
            <br/>
        </xsl:for-each>
        </ul>
        <hr/>
    <xsl:for-each select="codingstandard/section">
        <div class="section">
            <div class="sectiontitle">
                <a>
                    <xsl:attribute name="name"><xsl:value-of select="@title"/></xsl:attribute>
                    <xsl:value-of select="@title"/>
                </a>
            </div>
        <xsl:apply-templates select="item"/>
        </div>
    </xsl:for-each>
    </body>
    </html>
</xsl:template>


<xsl:template match="item">
    <div class="item">
        <div class="title">
            <a>
                <xsl:attribute name="name">Item-<xsl:value-of select="translate(@title,' ','')"/></xsl:attribute>
                <xsl:value-of select="@title"/>
            </a>
        </div>
        <xsl:apply-templates/>
    </div>
</xsl:template>


<xsl:template match="snippet">
    <div class="snippet">
        <pre>
        <xsl:apply-templates/>
        </pre>
    </div>
</xsl:template>


<xsl:template match="blankline">
    <br/>
</xsl:template>


<xsl:template match="*">
    <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
    </xsl:copy>
</xsl:template>

</xsl:stylesheet>

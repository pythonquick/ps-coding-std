# Coding Standard

This repo shares a collection of proposed coding standards/conventions at my services team, back in 2008.

The entries are contained in a XML file.
The neat thing is that the formatted output: https://innernet.io/share/codingstd.xml
happens on the fly via XSL. In fact there are only three files involved:

* codingstd.xml
* codingstd.xsl
* codingstd.css
